image := "archdriver:latest"

# Interactive menu to choose recipe
default:
  @just --choose

# Run the configuration bootstrap script
run:
  #!/bin/bash
  ./run

# Test the configuration bootstrap script
test:
  #!/bin/bash
  docker buildx build -t {{image}} -f docker/Dockerfile .
  docker run -it {{image}} ./test

# Run a shell inside your container
shell:
  #!/bin/bash
  docker buildx build -t {{image}} -f docker/Dockerfile .
  docker run -it {{image}} bash

# # List installed rust packages
# cargo:
#   #!/bin/bash
#   cargo install --list
#
